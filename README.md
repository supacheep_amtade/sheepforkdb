# README #

As its name, SheepForkDB has objective and requirement as its original, [SheepDB](https://bitbucket.org/supacheep_amtade/sheepdb).

So, this README is also the same. In this repository, we apply a new parallelize mechanism. 

### What is this SheepdForkDB for? ###

* Make for a distributed search engine 
* Optimize for a low-powered platform
* Test idea for a search engine on a low-powered plantform

### How do I get set up? ###

* [LevelDB](https://github.com/google/leveldb)
* libssl-dev
* libneon27
* ptheard