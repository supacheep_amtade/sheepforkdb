/*
 * benchmark.cpp
 *
 *  Created on: Mar 6, 2015
 *      Author: ppus
 */

#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "../net/util_ssl.h"
#include "../utils/alldef.hpp"
#include "../utils/demon_utils.hpp"	/* for read_config() and get_ip() */

int main(int argc, char* argv[]){
	/* initial ssl client */
	SSL_CTX *ctx;
	int server;
	SSL *ssl;
	char *buf;
	int bytes;
	char *hostname = (char*)malloc(sizeof(char)*20);
	int port;

	char *conffile = (char*)malloc(sizeof(char)*256);

	strcat(conffile, "conf/sheep_config.json");
	if (argc == 3){
		strcat(conffile, ".");
		strcat(conffile, argv[2]);
	}

	printf("config = %s\n",conffile);
	read_config(conffile);
	hostname = get_master();
	port = PORT_SSL;

	ctx = InitCTX();
	server = OpenConnection(hostname, port);
	ssl = SSL_new(ctx);						/* create new SSL connection state */
	SSL_set_fd(ssl, server);				/* attach the socket descriptor */
	if ( SSL_connect(ssl) == FAIL )			/* perform the connection */
		ERR_print_errors_fp(stderr);
	else{
		printf("Benchmark with %s encryption\n", SSL_get_cipher(ssl));
		ShowCerts(ssl);							/* get any certs */

		/* Open benchmark input file */
		FILE * fp;
		char * line;
        size_t line_len = 0;
        ssize_t read;

        fp = fopen(argv[1], "r");
        if (fp == NULL) exit(EXIT_FAILURE);

        /* timer */
        clock_t before = clock();

        int count = 0;
        /* Read Benchmark */
        buf = (char *)malloc(sizeof(char)*(ALLBUF));
		while((read = getline(&line, &line_len, fp)) != -1){
			buf[0] = '\0';
			SSL_write(ssl, line, strlen(line));			/* encrypt & send message */

			bytes = SSL_read(ssl, buf, sizeof(char)*ALLBUF);	/* get reply & decrypt */
			buf[bytes] = '\0';

//			printf("count : %d\nrecv : %s\n", ++count,buf);
		}
		free(buf);
		
		before = clock() - before;
		printf("Time taken %f seconds\n",
		  ((float) before)/CLOCKS_PER_SEC);
		
		fclose(fp);

		SSL_free(ssl);								/* release connection state */
	}
	close(server);									/* close socket */
	SSL_CTX_free(ctx);								/* release context */

	return 0;
}


