/*
 * sheepd.cpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#include "string.h"
#include "stdio.h"
#include "pthread.h"	/* for pthread */

#include "../net/util_net.h"	/* for DieWithError() */
#include "../net/master.hpp"	/* for master function */
#include "../utils/demon_utils.hpp"	/* for read_config() and get_ip() */

int main(int argv, char* argc[]){
	char* filename = "conf/sheep_config.json";
	bool isMaster = false; bool isWorker = false;

	read_config(filename);

	if ( argv == 2 ){
		printf("Debug mode\n");
		EnableDebug(true);
	}

	if (strcmp(get_ip(), get_master()) == 0){
		pthread_t masterSSLThread;
		for(;;){
			if (pthread_create(&masterSSLThread, NULL, MasterSSLListen, NULL) != 0)
				DieWithError("masterSSLThread: pthread_create() failed");

			pthread_join(masterSSLThread, NULL);		/* wait till master-cli thread's done */
		}
	}

	printf("close master node.\n");

	return 1;
}
