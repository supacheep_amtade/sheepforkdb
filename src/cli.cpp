/*
 * cli.cpp
 *
 *  Created on: Mar 2, 2015
 *      Author: ppus
 */

#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>

#include "../net/util_ssl.h"
#include "../utils/alldef.hpp"

int main(int argc, char* argv[]){
	/* initial ssl client */
	SSL_CTX *ctx;
	int server;
	SSL *ssl;
	char buf[ALLBUF];
	int bytes;
	char *hostname, *port;
	bool debug = false;

	if ( argc < 3 ){
		printf("usage: %s <hostname> <portnum>\n", argv[0]);
		exit(0);
	}else if ( argc == 3 ){
		hostname = argv[1];
		port = argv[2];
	}else if ( argc == 4 ){
		hostname = argv[1];
		port = argv[2];
		debug = true;
		printf("Debug mode\n");
	}


	ctx = InitCTX();
	server = OpenConnection(hostname, atoi(port));
	ssl = SSL_new(ctx);						/* create new SSL connection state */
	SSL_set_fd(ssl, server);				/* attach the socket descriptor */
	if ( SSL_connect(ssl) == FAIL )			/* perform the connection */
		ERR_print_errors_fp(stderr);
	else{
		char* str = (char*)malloc(sizeof(char)*256);			/* str for sent */
		printf("Connected with %s encryption\n", SSL_get_cipher(ssl));
		ShowCerts(ssl);							/* get any certs */
		printf ("CLI - SheepDB_Beta_0.42\nQuery Command in SheepQL\n"
					" put \"key\" \"value\"\n del \"key\"\n"
					" get \"key\"\n use \"dbname\"\n");

		char addr[256];
		strcpy(addr,"db/");
		while(1){
			printf(addr);
			printf("> ");
			fgets(str, 255, stdin);
			str[strlen(str)-1] = '\0';
			SSL_write(ssl, str, strlen(str));			/* encrypt & send message */
			bytes = SSL_read(ssl, buf, sizeof(buf));	/* get reply & decrypt */
			buf[bytes] = '\0';
			if (debug) printf("Received: \"%s\"\n", buf);
			/* Extract command at first word */
			char* ptr = str+1; char* ptr2; char* out; int len = 0;
			while (*ptr!=' ' && *ptr && ++len) *ptr++;
			out = (char*)malloc(len + 1);
			if (!out) return '\0';
			ptr = str; ptr2 = out;
			while (*ptr != ' ' && *ptr){ *ptr2++ = *ptr++;}
			*ptr2 = 0; ptr++;
			if (strcmp(out,"use") == 0){
				/* WILL IMPLEMENT USER ADDRESS */
//				addr=realloc(addr,[bytes]);
//				strncpy(addr, buf, bytes);
			}
			if (strcmp(str, "exit\n") == 0) {
				bytes = SSL_read(ssl, buf, sizeof(buf));	/* get reply & decrypt */
				buf[bytes] = 0;
//				printf("Received: \"%s\"\n", buf);
				break;
			}
		}
		SSL_free(ssl);								/* release connection state */
	}
	close(server);									/* close socket */
	SSL_CTX_free(ctx);								/* release context */
	return 0;
}
