/*
 * benchmark.cpp
 *
 *  Created on: Mar 6, 2015
 *      Author: ppus
 */

#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "../net/util_ssl.h"
#include "../utils/alldef.hpp"
#include "../utils/demon_utils.hpp"	/* for read_config() and get_ip() */

int main(int argc, char* argv[]){
	/* initial ssl client */
	SSL_CTX *ctx;
	int server;
	SSL *ssl;
	char *buf;
	int bytes;
	char *hostname = (char*)malloc(sizeof(char)*20);
	int port;

	char *conffile = (char*)malloc(sizeof(char)*256);

	strcat(conffile, "/home/raspib/tempSheep/sheepforkdb/conf/sheep_config.json");

	read_config(conffile);
	hostname = get_master();
	port = PORT_SSL;

	ctx = InitCTX();
	server = OpenConnection(hostname, port);
	ssl = SSL_new(ctx);						/* create new SSL connection state */
	SSL_set_fd(ssl, server);				/* attach the socket descriptor */
	if ( SSL_connect(ssl) == FAIL )			/* perform the connection */
		ERR_print_errors_fp(stderr);
	else{
		char * line = (char *)malloc(sizeof(char)*CMDBUFSIZE);
		strcpy(line, "get \"\0");
		strcat(line, argv[1]);
		strcat(line, "\"\0");

        /* timer */
        clock_t before = clock();

        int count = 0;
        /* Read Benchmark */
        buf = (char *)malloc(sizeof(char)*(ALLBUF));
                buf[0] = '\0';
		SSL_write(ssl, line, strlen(line));
		bytes = SSL_read(ssl, buf, sizeof(char)*ALLBUF);	/* get reply & decrypt */

		buf[bytes] = '\0';
		printf("%s\n", buf);

		free(buf);
                free(line);		
		before = clock() - before;

		SSL_free(ssl);								/* release connection state */
	}
	close(server);									/* close socket */
	SSL_CTX_free(ctx);								/* release context */

	return 0;
}
