/*
 * master.hpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#ifndef MASTER_HPP_
#define MASTER_HPP_

#ifdef __cplusplus
extern "C"
{
#endif

#include "util_ssl.h"
#include "../utils/alldef.hpp"

typedef struct SentThreadArg
{
	char* cmd;
	char* servIP;
	char* reply;
	int threadNum;
	SSL* ssl;
} SentThreadArg;

void *ThreadMaster(void *arg);		/* Master work of a thread */
void* MasterListen(void* arg);		/* Master program for pthread */
void* MasterSSLListen(void* arg);	/* Master program for ssl thread */
void MasterRecvResult(int clntSock);	/* Master listen to result */
void* MasterSend(void* arg);	/* Master send cmd implement in thread way */
void* Servlet(void* arg);		/* SSL servlet (contexts can be shared) the connection -- threadable */
void EnableDebug(bool d);		/* enable debug mode */
int mergeReply(char*&, char*);	/* merge reply pass pointer of answer and update in this function */
#ifdef __cplusplus
}
#endif

#endif /* MASTER_HPP_ */
