/*
 * master.cpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>

#include "pthread.h"
#include "stdlib.h"
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include "unistd.h"		/* for close() */
#include <sys/socket.h> /* for socket(), bind(), and connect() */

#include "util_net.h"
#include "master.hpp"
#include "../utils/alldef.hpp"
#include "../utils/demon_utils.hpp"

bool debug1 = false;
static volatile int* threadLock;		/* array of thread status flags */
static volatile int sendLock = 0;

void EnableDebug(bool d){
	debug1 = d;
}

void* MasterListen(void* arg){
	int servSock;                    /* Socket descriptor for server */
	int clntSock;                    /* Socket descriptor for client */
	unsigned short echoServPort;     /* Server port */
	pthread_t threadID;              /* Thread ID from pthread_create() */
	struct ThreadArgs *threadArgs;   /* Pointer to argument structure for thread */

	echoServPort = PORT_MASTER;

	servSock = CreateTCPServerSocket(echoServPort);

	for (;;) /* run forever */{
		clntSock = AcceptTCPConnection(servSock);

		/* Create separate memory for client argument */
		if ((threadArgs = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs)))
			   == NULL)
			DieWithError("malloc() failed");

                threadArgs -> clntSock = clntSock;

		/* Create client thread */
		if (pthread_create(&threadID, NULL, ThreadMaster, (void *) threadArgs) != 0)
			DieWithError("net: pthread_create() failed");
	}
	/* NOT REACHED */
}

void* MasterSSLListen(void* arg){
	SSL_CTX *ctx;
	int serv;
	int portnum = PORT_SSL;
	pthread_t sslthreadID;              /* Thread ID from pthread_create() */
	struct SSLThreadArgs *sslthreadArgs;   /* Pointer to argument structure for thread */


	ctx = InitServerCTX();		/* initialize SSL */
	LoadCertificates(ctx, ".ssl/sheepdb.crt", ".ssl/sheepdb.key");	/* load certs */
	serv = OpenListener(portnum);				/* create server socket */

	for(;;) /* run forever */{
		struct sockaddr_in addr;
		unsigned int len = sizeof(addr);
		SSL *ssl;

		int client = accept(serv, (struct sockaddr *) &addr, &len);		/* accept connection as usual */
		printf("Connection: %s:%d\n",
			inet_ntoa(addr.sin_addr), ntohs(addr.sin_port)); /* debug connection */
		ssl = SSL_new(ctx);         					/* get new SSL state with context */
		SSL_set_fd(ssl, client);						/* set connection socket to SSL state */

		/* Create separate memory for client argument */
		if ((sslthreadArgs = (struct SSLThreadArgs *) malloc(sizeof(struct SSLThreadArgs)))
			   == NULL)
			DieWithError("malloc() failed");
		sslthreadArgs -> ssl_t = ssl;

		/* Create client thread service connection */
		if (pthread_create(&sslthreadID, NULL, Servlet, (void *) sslthreadArgs) != 0)
			DieWithError("ssl: pthread_create() failed");
	}
	close(serv);										/* close server socket */
	SSL_CTX_free(ctx);									/* release context */
}

void* ThreadMaster(void* arg){
	int clntSock = ((struct ThreadArgs *) arg) -> clntSock;	/* Socket descriptor for client connection */
	free(arg);              /* Deallocate memory for argument */

	MasterRecvResult(clntSock);

	/* Guarantees that thread resources are deallocated upon return */
	pthread_detach(pthread_self());
}

void MasterRecvResult(int clntSock){
	char echoBuffer[RCVBUFSIZE];        /* Buffer for echo string */
	int recvMsgSize;                    /* Size of received message */

	/* Receive message from client */
	if ((recvMsgSize = recv(clntSock, echoBuffer, RCVBUFSIZE, 0)) < 0)
		DieWithError("recv() failed");

	/* Send received string and receive again until end of transmission */
	while (recvMsgSize > 0){      /* zero indicates end of transmission */
		/* See if there is more data to receive */
		if ((recvMsgSize = recv(clntSock, echoBuffer, RCVBUFSIZE, 0)) < 0)
			DieWithError("recv() failed");
	}

	close(clntSock);    /* Close client socket */
}

void* MasterSend(void* arg){
	struct SentThreadArg *targ = (struct SentThreadArg*) arg;
	int sock;                        /* Socket descriptor */
	struct sockaddr_in workerAddr; /* worker server address */
	unsigned short workerPort = PORT_WORKER;     /* worker server port */
	char* resultBuffer = (char *)malloc(sizeof(char) * (RCVBUFSIZE + 1));     /* Buffer for result */
	int recvMsgSize;				/* msg size */
	targ -> reply = (char *)malloc(sizeof(char) * ALLBUF);	/* returned result */
	unsigned int cmdLen;      /* Length of string to echo */
	int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv()
										and total bytes read */
        

	/* Create a reliable, stream socket using TCP */
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		DieWithError("socket() failed");

	/* Construct the server address structure */
	memset(&workerAddr, 0, sizeof(workerAddr));     /* Zero out structure */
	workerAddr.sin_family      = AF_INET;             /* Internet address family */
	workerAddr.sin_addr.s_addr = inet_addr(targ->servIP);   /* Server IP address */
	workerAddr.sin_port        = htons(workerPort); /* Server port */

	/* Establish the connection to the echo server */
	if (connect(sock, (struct sockaddr *) &workerAddr, sizeof(workerAddr)) < 0)
		DieWithError("connect() failed");

	cmdLen = strlen(targ->cmd);          /* Determine input length */

	if(targ->cmd[cmdLen-1] == '\n')
		targ->cmd[cmdLen-1] = '\0';

	if(debug1)
		printf("cmd@master send: %s\ncmdLen : %d\n", targ->cmd, cmdLen);

//        printf("error before send\n");

	/* Send the cmd to the server */
	if (send(sock, targ -> cmd, cmdLen, 0) != cmdLen)
		DieWithError("send() sent a different number of bytes than expected");

	/* Receive the same string back from the server */
	totalBytesRcvd = 0;
	
//        printf("error before recv\n");

	/* Receive message from client */
	if ((recvMsgSize = recv(sock, targ->reply, ALLBUF, 0)) < 0){
                DieWithError("recv() failed");
        }

	if(debug1) printf("%s\n", targ->reply);
        
        if (targ->cmd[0] == 'g' && targ->cmd[1] == 'e' && targ->cmd[2] == 't' && strcmp(targ->reply, "nack") != 0)
            while (targ->reply[recvMsgSize-1] != '}')
                if ((recvMsgSize = recv(sock, targ->reply, ALLBUF, 0)) < 0){
                    DieWithError("recv() failed");
                }
        
        targ->reply[recvMsgSize] = '\0';

	close(sock);

	if(debug1) printf("%s\n", targ->reply);

	while(sendLock == 1);		/* check lock */
	sendLock = 1;				/* lock */
	SSL_write(targ -> ssl, targ -> reply, strlen(targ -> reply));
	sendLock = 0;				/* unlock */

        free(targ->reply);

        threadLock[targ->threadNum] = 0;		/* unlock */

	pthread_detach(pthread_self());		/* dealllocate resource when return */
        
	pthread_exit(NULL);
}

/* SSL servlet (contexts can be shared) */
/* Serve the connection -- threadable */
void* Servlet(void* arg){
        int count_line = 1;
        SSL* ssl = ((struct SSLThreadArgs *) arg)->ssl_t;
	free(arg);

	/* pthread init */
	struct SentThreadArg *targ[get_worker_len()];
	for(int i = 0 ; i < get_worker_len() ; ++i ){
		targ[i] = (SentThreadArg *)malloc(sizeof(SentThreadArg));
		targ[i]->cmd = (char *)malloc(sizeof(char)*CMDBUFSIZE);
		targ[i]->servIP = (char *)malloc(sizeof(char)*18);
		targ[i]->ssl = ssl;
		targ[i]->threadNum = i;
	}

    if ( SSL_accept(ssl) == FAIL )					/* do SSL-protocol accept */
        ERR_print_errors_fp(stderr);
    else{
        ShowCerts(ssl);								/* get any certificates */
        pthread_t threadID[get_worker_len()];
        char threadChecker[get_worker_len()];
        memset(threadChecker, 0, get_worker_len());

        char** buf = (char**)malloc(sizeof(char*) * get_worker_len());
        threadLock = (int*)malloc(sizeof(int) * get_worker_len());
        for (int i = 0; i < get_worker_len() ; i++){
        	buf[i] = (char*)malloc(sizeof(char) * CMDBUFSIZE);
        	threadLock[i] = 0;
        }

        int sd, bytes, nt = 0, out = 0;
        while(out == 0){
        	for (int nt = 0; nt < get_worker_len(); nt++){
        		if (threadLock[nt] == 1){ continue;}		/* checked wait for lock */
//                      while(threadLock[nt]);
                        bytes = SSL_read(ssl, buf[nt], CMDBUFSIZE);

				if ( bytes > 0 ){
					if (buf[nt][bytes-1] == '\n')
						buf[nt][bytes-1] = '\0';
					else
						buf[nt][bytes] = '\0';

//					printf("line:%d buf[%d] = %s\n",count_line++,nt,buf[nt]);
                                        if (strcmp(buf[nt],"exit") == 0 ) { out = 1; printf("close master node.\n"); break;}
					strcpy(targ[nt]-> cmd, buf[nt]);
					targ[nt]->cmd[bytes] = '\0';			/* double close string to ensure that close */
					strcpy(targ[nt]-> servIP, get_worker(nt));

					if (debug1) printf("Client msg: \"%s\" to ip: %s\n", targ[nt]-> cmd, get_worker(nt));

					if (targ[nt] -> cmd[0] == 'u' &&
							targ[nt] -> cmd[1] == 's' &&
								targ[nt] -> cmd[2] =='e'){
						int count_ntt = get_worker_len();
						int worker_len = get_worker_len();
						for (int nnt = nt; count_ntt > 0; count_ntt--){
							while(threadLock[nnt] == 1);		/* checked wait for lock */
							threadLock[nnt] = 1;		/* lock */

							strcpy(targ[nnt]-> cmd, buf[nt]);
							targ[nnt]->cmd[bytes] = '\0';			/* double close string to ensure that close */
							strcpy(targ[nnt]-> servIP, get_worker(nnt));		/* set dest ip */
							pthread_create(&threadID[nnt], NULL, MasterSend, (void *)targ[nnt]);


							nnt++;
							if (nnt == worker_len) nnt = 0;
						}
					}
					else {
						threadLock[nt] = 1;		/* lock */
						pthread_create(&threadID[nt], NULL, MasterSend, (void *)targ[nt]);
					}
				}
				else {
			//		printf("Input error, the program will close.\n");
			//		out = 1;
			//		ERR_print_errors_fp(stderr);
				}
        	}
        }
        /* free heap memory */
	for(int i = 0 ; i < get_worker_len() ; ++ i ){
                while(threadLock[i]==1);
//		free(targ[i] -> cmd);
//		free(targ[i] -> servIP);
//		SSL_free(targ[i] -> ssl);
//		free(targ[i]);
//		free(buf[i]);
	}
//	free(buf);
        
        SSL_write(ssl, "close connection.", strlen("close connection."));	/* send reply */
        sd = SSL_get_fd(ssl);							/* get socket connection */
        SSL_free(ssl);									/* release SSL state */
        close(sd);										/* close connection */
    }
	
    pthread_detach(pthread_self());
    pthread_exit(NULL);
}

int mergeReply(char *&dest, char* source){
	int end = strlen(source);
	if (source[0] == 'd' && source[1] == 'b'){
		strcat(dest, source);
		return 1;
	}
	else{
		source[end - 1] = '\0';
		source = source + 2;
		strcat(dest, source);
	}
	return 0;
}
