/*
 * worker.cpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>	/* for recv(), send() */
#include <unistd.h>		/* for close() */

#include "worker.hpp"
#include "util_net.h"
#include "../utils/alldef.hpp"
#include "../utils/code_generater.hpp"

CodeGenerater cg;			/* class CodeGenerater */
bool debug;

void WorkerProgram(bool dbg){
	debug = dbg;
        int wthreadNum = 2;
	int servSock;                    /* Socket descriptor for server */
	int clntSock;                    /* Socket descriptor for client */
	unsigned short echoServPort;     /* Server port */

	echoServPort = PORT_WORKER;

	servSock = CreateTCPServerSocket(echoServPort);

        for (;;) /* run forever */{
            clntSock = AcceptTCPConnection(servSock);

            WorkerProcess(clntSock);
        }
}

void WorkerProcess(int clntSock){
	int recvMsgSize;                    /* Size of received message */
        char *cmdBuffer = (char *)malloc(sizeof(char) * CMDBUFSIZE);	/* Buffer for data */

//      char cmdBuffer[CMDBUFSIZE];
        memset(cmdBuffer, 0, CMDBUFSIZE);

	/* Receive message from client */
	if ((recvMsgSize = recv(clntSock, cmdBuffer, CMDBUFSIZE, 0)) < 0)
		DieWithError("recv() failed");

//	printf("%s recv_size = %d\n", cmdBuffer, recvMsgSize);    /* Print a final linefeed */
        cmdBuffer[recvMsgSize] = '\0';
    	
	if (debug)
		printf("%s <- this is the recvd command\n",cmdBuffer);

        char* answer = (char *)malloc(sizeof(char) * ALLBUF);

        if (strcmp(cmdBuffer, "\n") != 0) { strcpy(answer,cg.execute(cmdBuffer));} /* execute query */

	if (strcmp(answer, "") == 0){
            strcpy(answer, "nack\0");
	}

	if (debug)
		printf("%s <- this is the answer\n",answer);

	/* Answer message back to server */
        while (send(clntSock, answer, strlen(answer), 0) != strlen(answer));


	/* free heap memmory */
	free(cmdBuffer);

	close(clntSock);    /* Close client socket */
}
