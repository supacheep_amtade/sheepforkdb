/*
 * worker.hpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#ifndef WORKER_HPP_
#define WORKER_HPP_

#ifdef __cplusplus
extern "C"
{
#endif

/* worker group */
void WorkerProgram(bool debug);		/* Worker program */
void* ThreadWorker(void *arg);		/* Worker work of a thread */
void WorkerProcess(int clntSock);	/* Worker handle process */

#ifdef __cplusplus
}
#endif

#endif /* WORKER_HPP_ */
