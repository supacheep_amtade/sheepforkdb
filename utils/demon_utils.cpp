/*
 * demon_utils.cpp
 *
 *  Created on: Mar 4, 2015
 *      Author: ppus
 */
#include "stdio.h"
#include "string.h"		/* for strcmp(), strerrno() */
#include "stdlib.h"		/* for free() */
#include <unistd.h>		/* for close() */
#include <sys/socket.h> /* for socket(), bind(), and connect() */
#include <sys/types.h>	/* for const variable */
#include <sys/ioctl.h>	/* for ioctl() */
#include <netinet/in.h>	/* for inet */
#include <net/if.h>		/* for if family */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include "../utils/cJSON.h"		/* for json utilities */
#include "string.h"		/* for strcmp(), strerrno() */
#include "errno.h"		/* for errno */
#include "demon_utils.hpp"

/* global variable */
char* master;		/* Master node */
char** worker;		/* Array of worker nodes */
int port;			/* Port server */
int worker_len;		/* Worker number followed by config file */

/* global variable */
char* get_master() { return master; }		/* Master node */
char* get_worker(int i) { return worker[i]; }	/* Array of worker nodes */
int get_port() { return port; }			/* Port server */
const int get_worker_len() { return (const int)worker_len; }		/* Worker number followed by config file */

int read_config(char* filename){
	/* Open configure file */
	FILE *f = fopen(filename,"rb");

	if (f == NULL) {
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error opening the file: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	fseek(f,0,SEEK_END); double len = ftell(f); fseek(f,0,SEEK_SET);

	char *data = (char*)malloc(len+1); fread(data,1,len,f); fclose(f);

	/* Read JSON */
	char *out; cJSON *json; worker_len = 1; int i;
	json = cJSON_Parse(data);
	if (!json) {
		fprintf(stderr,"Error before: [%s]\n", cJSON_GetErrorPtr());
		return 0;
	} else {
		json = json->child;
		while (json){
			/* Parse all config from file by json step */
			if (strcmp(json->string,"master") == 0){
				master = json->valuestring; json = json->next;
			}else if (strcmp(json->string,"port") == 0){
				port = json->valueint; json = json->next;
			}else if (strcmp(json->string,"worker") == 0){
					worker_len = cJSON_GetArraySize(json);
					cJSON* tempJSON = json->child;
					worker = (char **)malloc((sizeof(char *)*worker_len));
					for (i = 0; i < worker_len; i++){
						worker[i] = (char *)malloc(sizeof(char)*strlen(tempJSON->valuestring) + 1);
						worker[i] =  tempJSON->valuestring;
						tempJSON = tempJSON->next;
					}
					free(tempJSON);
					json = json->next;
			}else json = json->next;
		}
		out = cJSON_Print(json);
		cJSON_Delete(json);
	}
	free(data);
	return 1;
}

char* get_ip(){
	int fd;
	struct ifreq iface;

	fd = socket(AF_INET, SOCK_DGRAM, 0);

	/* Get IP address */
	iface.ifr_addr.sa_family = AF_INET;

	/* Get IP from "eth0" */
	strncpy(iface.ifr_name, "eth0", IFNAMSIZ-1);

	ioctl(fd, SIOCGIFADDR, &iface);

	close(fd);

//	/* Print IP */
//	printf("%s\n", inet_ntoa(((struct sockaddr_in *)&iface.ifr_addr)->sin_addr));

	return inet_ntoa(((struct sockaddr_in *)&iface.ifr_addr)->sin_addr);
}

int checkWorker(){
	char* ip = get_ip(); int i;
	for (i = 0; i < worker_len; i++)
		if (strcmp(worker[i], ip) == 0){ return 1; }
	return 0;
}


