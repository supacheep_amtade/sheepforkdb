/*
 * counter.hpp
 *
 *  Created on: Feb 14, 2015
 *      Author: ppus
 */

#ifndef COUNTER_HPP_
#define COUNTER_HPP_

#include "db_doer.hpp"

class Counter : public DBDoer{

public:
	Counter(){ initialize("db/counter");}
	Counter(char* dblocation){ initialize(dblocation);}
//	~Counter(){};
	void count(char* db_key, char* db_value);
private:
	void print_error();
};

#endif /* COUNTER_HPP_ */
