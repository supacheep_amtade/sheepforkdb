/*
 * author_indexer.hpp
 *
 *  Created on: Feb 15, 2015
 *      Author: ppus
 */

#ifndef AUTHOR_INDEXER_HPP_
#define AUTHOR_INDEXER_HPP_

#include <string>

#include "db_doer.hpp"
#include "cJSON.h"

class AuthorIndexer : public DBDoer {
public:
	AuthorIndexer(){ initialize("db/author");}
	AuthorIndexer(char* dblocation){ initialize(dblocation);}
//	~AuthorIndexer(){}
	void extract(char* dbkey, char* dbvalue);
private:
	std::string generate_result(char* dbvalue);
	cJSON *generate_record(char* author, char* dbkey, char* dbvalue);
};




#endif /* AUTHOR_INDEXER_HPP_ */
