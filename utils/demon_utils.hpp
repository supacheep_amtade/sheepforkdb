/*
 * demon_utils.hpp
 *
 *  Created on: Mar 4, 2015
 *      Author: ppus
 */

#ifndef DEMON_UTILS_HPP_
#define DEMON_UTILS_HPP_

#ifdef __cplusplus
extern "C"
{
#endif

/* global variable */
char* get_master();		/* Master node */
char* get_worker(int);		/* Array of worker nodes */
int get_port();			/* Port server */
const int get_worker_len();		/* Worker number followed by config file */
int checkWorker();	/* check is this worker */
int read_config(char* filename);	/* read config file */
char* get_ip();		/* get ip from host */

#ifdef __cplusplus
}
#endif

#endif /* DEMON_UTILS_HPP_ */
