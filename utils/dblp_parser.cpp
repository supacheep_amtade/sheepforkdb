/*
 * dbpl_parser.cpp
 *
 *  Created on: Feb 13, 2015
 *      Author: ppus
 */
#include "stdlib.h"
#include "errno.h"

#include "dblp_parser.hpp"
#include "cJSON.h"

extern int errno;

/* Read json string, parser, generate record, etc. */
bool DBLPParser::dojson(const char* data){
	/* Read JSON */
	char *out; cJSON *json;
	json = cJSON_Parse(data);
	if (!json) {
		fprintf(stderr,"Error before: [%s]\n", cJSON_GetErrorPtr());
		return false;
	} else {
		AuthorIndexer author_indexer;
		Counter counter;
		out = cJSON_Print(json);
		generate_record(out);
		cJSON_Delete(json);
		return true;
	}
}


/* Read a file, parse, render back, etc. */
bool DBLPParser::dofile(char* filename){
	FILE *f = fopen(filename,"rb");

	if (f == NULL) {
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error opening the file: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	fseek(f,0,SEEK_END); double len = ftell(f); fseek(f,0,SEEK_SET);

	char *data = (char*)malloc(len+1); fread(data,1,len,f); fclose(f);

	/* Read JSON */
	char *out; cJSON *json;
	json = cJSON_Parse(data);
	if (!json) {
		fprintf(stderr,"Error before: [%s]\n", cJSON_GetErrorPtr());
		free(data);
		return false;
	} else {
		out = cJSON_Print(json);
		generate_record(out);
		cJSON_Delete(json);
		free(data);
		return true;
	}
}

/* Generate DBLP record (key, value) */
void DBLPParser::generate_record(char *json_string){
	/* json utilities */
	cJSON *root,*fmt;
	root = cJSON_Parse(json_string);
	fmt = root->child;

	AuthorIndexer author_indexer(dbauthor);
//	Counter counter;
	while (fmt){
		/* add type to record */
		cJSON_AddStringToObject(fmt, "type", fmt->string);

		/* generate key by year of @mdate combine with @key */
		char* generated_key = new char[strlen(cJSON_GetObjectItem(fmt, "@mdate")->valuestring)
		                               + strlen(cJSON_GetObjectItem(fmt, "@key")->valuestring)
		                               + 2];
		*generated_key = '\0';
		char* year = new char[10]; *year = '\0';
		strcpy(year, cJSON_GetObjectItem(fmt, "@mdate")->valuestring);
		strcat(generated_key, strtok(year, "-"));
		strcat(generated_key, "/");
		strcat(generated_key, cJSON_GetObjectItem(fmt, "@key")->valuestring);
//		printf("%s\n", generated_key); // debug key

		/* replace "title" element */
		/* handle puzzle in string of title */
		cJSON* t_fmt = fmt;
		char* out = new char[10240]; *out = '\0';// reverse space for our char[10240]
		if (cJSON_ExistItem(t_fmt,"title"))
			if (cJSON_GetObjectItem(t_fmt,"title")->type != cJSON_String){
				t_fmt = cJSON_GetObjectItem(t_fmt,"title");
				t_fmt = cJSON_GetObjectItem(t_fmt,"#text");
				if (t_fmt->type == cJSON_String){
//					printf("%s\n", t_fmt->valuestring); // debug output
					strcpy(out, t_fmt->valuestring);
				}
				else if (t_fmt->type == cJSON_Array){
					cJSON* c = t_fmt->child;
					while (c){
						strcat(out, c->valuestring);
						c = c->next;
					}
//					printf("%s\n", out); // debug outputS
				}
				cJSON_ReplaceItemInObject(fmt, "title", cJSON_CreateString(out));
//				printf("%s\n", cJSON_GetObjectItem(fmt, "title")->valuestring); // debug new title
			}

		/* generated json value */
		char* json_value = cJSON_Print(fmt);
		/* put key:value to db */
		putdb(generated_key, json_value);

		/* (optional) extract and generate author atrribute */
		author_indexer.extract(generated_key, json_value);
//		/* (optional) count word in title */
//		counter.count(generated_key, json_value);


		/* goto next child */
		fmt = fmt->next;
		free(generated_key);
	}
//	printf("Done, parsed\n");
}
