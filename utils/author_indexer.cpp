/*
 * author_indexer.cpp
 *
 *  Created on: Feb 15, 2015
 *      Author: ppus
 */
#include "author_indexer.hpp"
#include "cJSON.h"

char* removeline(char* line){
	char t[strlen(line)];
	strcpy(t,line);
	for (int i = 0; i < strlen(t); i++){
		if (t[i] == '\n' || t[i] == '\"') t[i] = ' ';

	}
	return t;
}

void AuthorIndexer::extract(char* dbkey, char* dbvalue){
	cJSON* json = cJSON_Parse(dbvalue);

	/* if not exist attribute in json, then stop function */
	if (!cJSON_ExistItem(json, "author")) return;

	/* Extract author's name from array */
	int i = 0;
	json = cJSON_GetObjectItem(json, "author");
	if (json != NULL){
//	printf("%s\n", dbvalue);
		int array_size = cJSON_GetArraySize(json);
		for (int i = 0; i < array_size; i++){
			char* author = cJSON_GetArrayItem(json, i)->valuestring;
//			printf("get \"%s\"\n", author);
//			printf("put \"%s\" \"%s\"\n", author
//					, removeline(cJSON_Print(generate_record(author, dbkey, dbvalue)))); // debug generate record
			if (author != NULL)
				putdb(author, cJSON_Print(generate_record(author, dbkey, dbvalue)));
		}
	}
}

cJSON *AuthorIndexer::generate_record(char* author, char* dbkey, char* dbvalue){
	std::string value;cJSON* json;
	if (getdb(author, value).IsNotFound()){
		json = cJSON_CreateObject();
		cJSON_AddItemToObject(json, dbkey, cJSON_CreateString(generate_result(dbvalue).c_str()));
	}else{
		json = cJSON_Parse(value.c_str());
		if (cJSON_ExistItem(json, dbkey))
			cJSON_ReplaceItemInObject(json, dbkey, cJSON_CreateString(generate_result(dbvalue).c_str()));
		else
			cJSON_AddItemToObject(json, dbkey, cJSON_CreateString(generate_result(dbvalue).c_str()));
	}
//	printf("%s\n", cJSON_Print(json));
	return json;
}

std::string AuthorIndexer::generate_result(char* dbvalue){
	std::string result;
	cJSON* json = cJSON_Parse(dbvalue);
	cJSON* next = cJSON_GetObjectItem(json, "author");
	int array_size = cJSON_GetArraySize(next);
	for (int i = 0; i < array_size; i++){
//		printf("%s\n", cJSON_Print(next));
		if (cJSON_GetArrayItem(next, i)->type == cJSON_String){
			result += cJSON_GetArrayItem(next, i)->valuestring;
			if ( i == array_size - 2 || i == 0)
				result += "\n";
			else
				result += ", ";
		}
	}

	if (cJSON_ExistItem(json, "title")){
		result += "<b>";
		result += cJSON_GetObjectItem(json, "title")->valuestring;
		result += "<\b>, ";
	}
	if (cJSON_ExistItem(json, "journal")){
		result += cJSON_GetObjectItem(json, "journal")->valuestring;
		result += ", ";
	}
	if (cJSON_ExistItem(json, "booktitle")){
		result += cJSON_GetObjectItem(json, "booktitle")->valuestring;
		result += ", ";
	}
	if (cJSON_ExistItem(json, "page")){
		result += cJSON_GetObjectItem(json, "page")->valuestring;
	}
	result += ".";
	return result;
}


