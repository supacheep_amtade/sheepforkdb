/*
 * counter.cpp
 *
 *  Created on: Feb 14, 2015
 *      Author: ppus
 */
#include "counter.hpp"
#include "cJSON.h"

void Counter::count(char* db_key, char* db_value){
	/* Initial JSON */
	cJSON* json; char* out = new char[256]; // reverse space for our char[256]
	*out = '\0';
	char* title;
	/* parse JSON form db_iteration */
	json = cJSON_Parse(db_value);
	if (cJSON_ExistItem(json,"title")){
		title = cJSON_GetObjectItem(json, "title")->valuestring;
//		printf("%s\n", title); // debug title
	} else {
		return;
	}

	/* leveldb initialize */
	leveldb::WriteOptions writeOptions;
	leveldb::ReadOptions readOptions;

	/* mapping */
	cJSON* temp_json = cJSON_CreateObject();
	char* tok_ptr;
	std::string object_value;
	tok_ptr = strtok(title, " ,-.&:;()/\'\\");
	json = cJSON_CreateObject();
	while (tok_ptr != NULL){
		if (cJSON_ExistItem(json, db_key)){
			int json_result = cJSON_GetObjectItem(json, db_key)->valueint;
			cJSON_ReplaceItemInObject(json, db_key, cJSON_CreateNumber(json_result + 1));
//			printf("%s\n", cJSON_Print(json));
		}else{
			cJSON_AddItemToObject(json, db_key, cJSON_CreateNumber(1));
		}
		tok_ptr = strtok(NULL, " ,-.&:;()/\'\\");
	}

	/* put value from temp_json to db */
	cJSON* c = temp_json->child;
	while (c){
		if (cJSON_ExistItem(json, db_key)){
			cJSON_ReplaceItemInObject(json, db_key, cJSON_GetObjectItem(temp_json,db_key));
		}else{
			cJSON_AddItemToObject(json, db_key, cJSON_GetObjectItem(temp_json,db_key));
		}
		db->Put(writeOptions, c->string, cJSON_Print(json));
	}
}

void Counter::print_error(){
	printf("++++++++++++++++++++++++Warning!!++++++++++++++++++++++++\n");
	printf("There are something wrong in title json when make_map \n");
}



