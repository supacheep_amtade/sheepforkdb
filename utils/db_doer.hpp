/*
 * db_doer.hpp
 *
 *  Created on: Feb 13, 2015
 *      Author: ppus
 */

#ifndef DB_DOER_HPP_
#define DB_DOER_HPP_

#include "leveldb/db.h"
#include "leveldb/options.h"
#include "leveldb/status.h"

class DBDoer {

public:
	DBDoer(){ ready = false;};
	DBDoer(char* dblocation){ initialize(dblocation); };	/* construct with db location */
	~DBDoer(){ delete db;};
	bool ready;		/* db initial status */
	leveldb::Status initialize(char* dblocation);			/* initialize by db folder location */
	leveldb::Status putdb(char* key, char* value);			/* put record by key and value */
	leveldb::Status getdb(char* key, std::string &value);	/* get record by key and pass output string by reference */
	leveldb::DB* db;
};


#endif /* DB_DOER_HPP_ */
