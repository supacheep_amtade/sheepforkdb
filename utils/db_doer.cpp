#include "db_doer.hpp"
#include "alldef.hpp"

leveldb::Status DBDoer::initialize(char* dblocation){
	if (ready) delete db;

	leveldb::Options dboption;
	dboption.create_if_missing = true;

	leveldb::Status dbstatus;
	dbstatus = leveldb::DB::Open(dboption, dblocation, &db);

	/* DB file Location */
	if (false == dbstatus.ok()){
		fprintf(stderr, "Unable to open/create test database '%s'\n", dblocation );
		fprintf(stderr, "%s\n", dbstatus.ToString().c_str());
		return dbstatus;
	}else ready = true;

	return dbstatus;
}

leveldb::Status DBDoer::putdb(char* key, char* value){
	return db->Put(leveldb::WriteOptions(), leveldb::Slice(std::string(key)), leveldb::Slice(std::string(value)));
}

leveldb::Status DBDoer::getdb(char* key, std::string &value){
	return db->Get(leveldb::ReadOptions(), leveldb::Slice(std::string(key)), &value);
}

//dblp-0856.xml
