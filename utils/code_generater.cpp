/*
 * code_generater.cpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#include <string>
#include "stdlib.h"

#include "alldef.hpp"
#include "code_generater.hpp"

char* CodeGenerater::execute(char *str){
	char* ptr = str+1; char* ptr2; char* out; int len = 0;
	char msg_out[ALLBUF] = {0};
        output = "";

        /* Check string type */
	if (*ptr == '\"') return parse_string(str);

	while (*ptr!=' ' && *ptr && ++len) *ptr++;

	out = (char*)malloc(len + 1);
	if (!out) return '\0';
	ptr = str; ptr2 = out;

	/* Extract command at first word */
	while (*ptr != ' ' && *ptr){ *ptr2++ = *ptr++;}
	*ptr2 = 0; ptr++;

	if (strcmp(out,"put") == 0){
		char* key = parse_string(ptr);
		if (key == NULL){
			strcat(msg_out, "Need key and value for command put\n "
					" eg. put \"key\" \"value\"");	/* assembly error message */
//			printf("Need key and value for command put\n "
//				" eg. put \"key\" \"value\"\n");
		}
		char* value = parse_string(ptr);
		if (value != NULL){
			/* dbDoer put data here */
			if (ready){
				db->Put(leveldb::WriteOptions(), key, value);
				strcat(msg_out, "put ");
				strcat(msg_out, key);
				strcat(msg_out, ", done");
			}else{
				strcat(msg_out, "Need to select database first");	/* assembly error message */
//				printf("Need to select database first\n");
			}
		}else{
			strcat(msg_out, "Need value for command put\n  eg. put \"key\" "
					"\"value\"");
//			printf("Need value for command put\n  eg. put \"key\" "
//				"\"value\"\n");
		}
	}else if (strcmp(out,"get") == 0){
		char* key = parse_string(ptr);
		if (key == NULL){
			strcat(msg_out, "Need key to search for command get "
					" eg. get \"key\"\n");
//			printf("Need key to search for command get\n "
//				" eg. get \"key\"\n");
		}else{
			/* dbDoer get data here */
                        initialize("db/author");
			if (db != NULL){
				db->Get(leveldb::ReadOptions(), key, &output);
				strcat(msg_out, output.c_str());
//				printf("%s\n", output.c_str());
			}else{
				strcat(msg_out, "Need to select database first");
//				printf("Need to select database first\n");
			}
		}
	}else if (strcmp(out,"use") == 0){
		char* name = parse_string(ptr);
		if (name == NULL){
			strcat(msg_out, "Need name of "
					"database for command use\n  eg. use \"name\"");
//			printf("Need name of "
//				"database for command use\n  eg. use \"name\"\n");
		}else{
			/* dbDoer initial new db here */
			char addr[256]; addr[0] = '\0';
			strcat(addr, "db/");
			strcat(addr, name);
			if (initialize(addr).ok()){
				current_addr = addr;
				strcat(msg_out, current_addr);
			}
		}
	}else if (strcmp(out,"del") == 0){
		char* key = parse_string(ptr);
		if (key == NULL){
			strcat(msg_out, "Need key to delete record for command get\n "
					" eg. get \"key\"");
//			printf("Need key to delete record for command get\n "
//				" eg. get \"key\"\n");
		}else{
			/* dbDoer get data here */
			if (db != NULL){
				db->Delete(leveldb::WriteOptions(), key);
			}else{
				strcat(msg_out, "Need to select database first");
//				printf("Need to select database first\n");
			}
		}
	}else{
		strcat(msg_out, "Need to select database first");
//		printf("No command or Wrong syntax\n eg. $COMMAND \"KEY\" ...\n");
	}

	str = ptr;

//	char* ptr = str+1; char* ptr2; char* out; int len = 0; char msg_out[1024];
	free(out);

	return msg_out;
}

char* CodeGenerater::parse_string(char *&str){
	char *ptr = str+1; char *ptr2; char *out; int len = 0;
	if (*str!='\"') { return '\0';}	/* not a string! */

	while (*ptr!='\"' && *ptr && ++len) if (*ptr++ == '\\') ptr++;	/* Skip escaped quotes. */

	out = (char*)malloc(sizeof(char)*(len + 1));
	if (!out) return '\0';
	ptr = str+1; ptr2 = out;

	/* Extract String */
	while (*ptr!='\"' && *ptr){
		if (*ptr!='\\') *ptr2++=*ptr++;
		else{
			ptr++;
			*ptr2++ = *ptr++;
		}
	}
	*ptr2 = 0;
	if (*ptr == '\"') ptr++;
	if (*ptr == ' ') ptr++;
	str = ptr;
	return out;
}


