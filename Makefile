NET=./net
SRC=./src
UTILS=./utils
BIN=./bin
CFLAGS=-O0 -g3 -Wall
DBFLAGS=-lleveldb
SSFLAGS=-lssl -lcrypto -lneon
C=gcc

ifeq "$(version)" "pi"
	CC=g++-4.8
else
	CC=g++
endif

all: $(BIN)/cli $(BIN)/sheepd $(BIN)/sheepwd $(BIN)/parsing-factory $(BIN)/benchmark $(BIN)/webquery

$(UTILS)/demon_utils.o: $(UTILS)/demon_utils.cpp
	$(CC) -c $(UTILS)/demon_utils.cpp -o $(UTILS)/demon_utils.o $(CFLAGS)

$(UTILS)/author_indexer.o: $(UTILS)/author_indexer.cpp
	$(CC) -c $(UTILS)/author_indexer.cpp -o $(UTILS)/author_indexer.o $(CFLAGS)

$(UTILS)/counter.o: $(UTILS)/counter.cpp
	$(CC) -c $(UTILS)/counter.cpp -o $(UTILS)/counter.o $(CFLAGS)

$(UTILS)/db_doer.o: $(UTILS)/db_doer.cpp
	$(CC) -c $(UTILS)/db_doer.cpp -o $(UTILS)/db_doer.o $(CFLAGS)

$(UTILS)/dblp_parser.o: $(UTILS)/dblp_parser.cpp
	$(CC) -c $(UTILS)/dblp_parser.cpp -o $(UTILS)/dblp_parser.o $(CFLAGS)

$(UTILS)/code_generater.o: $(UTILS)/code_generater.cpp
	$(CC) -c $(UTILS)/code_generater.cpp -o $(UTILS)/code_generater.o $(CFLAGS)

$(UTILS)/cJSON.o: $(UTILS)/cJSON.c
	$(C) -c $(UTILS)/cJSON.c -o $(UTILS)/cJSON.o $(CFLAGS)

$(NET)/util_net.o: $(NET)/util_net.c
	$(C) -c $(NET)/util_net.c -o $(NET)/util_net.o $(CFLAGS)

$(NET)/util_ssl.o: $(NET)/util_ssl.c
	$(C) -c $(NET)/util_ssl.c -o $(NET)/util_ssl.o $(CFLAGS)

$(NET)/master.o: $(NET)/master.cpp
	$(CC) -c $(NET)/master.cpp -o $(NET)/master.o $(CFLAGS)

$(NET)/worker.o: $(NET)/worker.cpp
	$(CC) -c $(NET)/worker.cpp -o $(NET)/worker.o $(CFLAGS)

$(BIN)/cli: $(NET)/util_ssl.o $(SRC)/cli.cpp
	$(CC) $(NET)/util_ssl.o $(SRC)/cli.cpp -o $(BIN)/cli -lssl -lcrypto -lneon $(CFLAGS)

$(BIN)/sheepd: $(UTILS)/cJSON.o $(NET)/util_ssl.o $(NET)/util_net.o $(NET)/master.o $(UTILS)/code_generater.o $(UTILS)/db_doer.o $(UTILS)/demon_utils.o $(SRC)/sheepd.cpp
	$(CC) $(UTILS)/cJSON.o $(NET)/util_ssl.o $(NET)/util_net.o $(NET)/master.o $(UTILS)/code_generater.o $(UTILS)/demon_utils.o $(UTILS)/db_doer.o $(SRC)/sheepd.cpp -o $(BIN)/sheepd $(SSFLAGS) $(DBFLAGS) -lpthread $(CFLAGS)

$(BIN)/sheepwd: $(UTILS)/cJSON.o $(NET)/util_ssl.o $(NET)/util_net.o $(NET)/worker.o $(UTILS)/code_generater.o $(UTILS)/db_doer.o $(SRC)/sheepwd.cpp
	$(CC) $(UTILS)/cJSON.o $(NET)/util_ssl.o $(NET)/util_net.o $(NET)/worker.o $(UTILS)/code_generater.o $(UTILS)/db_doer.o $(UTILS)/demon_utils.o $(SRC)/sheepwd.cpp -o $(BIN)/sheepwd $(SSFLAGS) $(DBFLAGS) -lpthread $(CFLAGS)

$(BIN)/parsing-factory: $(UTILS)/dblp_parser.o $(UTILS)/author_indexer.o $(UTILS)/counter.o $(UTILS)/cJSON.o $(UTILS)/db_doer.o $(SRC)/parsing_factory.cpp
	$(CC) $(UTILS)/db_doer.o $(UTILS)/cJSON.o $(UTILS)/dblp_parser.o $(UTILS)/author_indexer.o $(UTILS)/counter.o $(SRC)/parsing_factory.cpp -o $(BIN)/parsing-factory $(DBFLAGS) $(CFLAGS)

$(BIN)/benchmark: $(UTILS)/cJSON.o $(NET)/util_ssl.o $(UTILS)/demon_utils.o $(SRC)/benchmark.cpp
	$(CC) $(UTILS)/cJSON.o $(NET)/util_ssl.o $(UTILS)/demon_utils.o $(SRC)/benchmark.cpp -o $(BIN)/benchmark $(SSFLAGS) $(CFLAGS)

$(BIN)/webquery: $(UTILS)/cJSON.o $(NET)/util_ssl.o $(UTILS)/demon_utils.o $(SRC)/webquery.cpp
	$(CC) $(UTILS)/cJSON.o $(NET)/util_ssl.o $(UTILS)/demon_utils.o $(SRC)/webquery.cpp -o $(BIN)/webquery $(SSFLAGS) $(CFLAGS)

clean:
	rm -f ./*/*.o ./bin/*
